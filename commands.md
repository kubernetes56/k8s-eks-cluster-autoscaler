kubectl apply -f https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml



- kubectl edit deployment cluster-autoscaler -n kube-system

annotations:
  cluster-autoscaler.kubernetes.io/safe-to-evict: "false"
spec:
  containers:
  - command: 
    - --balance-similar-node-groups
    - --skip-nodes-with-system-pods=false
    image: k8s.gcr.io/autoscaling/cluster-autoscaler:v1.21.1

